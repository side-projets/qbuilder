#!/usr/bin/env bash

# On Mac OS, you can run `brew install antlr` to get it
if ! [[ -x "$(command -v antlr)" ]]; then
    echo 'antlr not installed' >&2
    exit 1
fi

cd src/main/resources

echo "Generating code ..."

antlr -o ../java/org/szhao/hello/autogen -package org.szhao.hello.autogen Hello.g4

antlr -o ../java/org/szhao/init/autogen -package org.szhao.init.autogen ArrayInit.g4

antlr -o ../java/org/szhao/expr/autogen -package org.szhao.expr.autogen Expr.g4

antlr -no-listener -visitor -o ../java/org/szhao/labeled/autogen -package org.szhao.labeled.autogen LabeledExpr.g4

echo " ==== handling QueryInURL grammar ==== "
antlr -o ../java/org/szhao/query/autogen -package org.szhao.query.autogen QueryInURL.g4

echo " ==== handling JSON grammar ==== "
antlr -o ../java/org/szhao/json/autogen -package org.szhao.json.autogen JSON.g4

cd ../../../

echo "Returning to `pwd`"