package org.szhao.hello;

import org.szhao.hello.autogen.HelloBaseListener;
import org.szhao.hello.autogen.HelloParser;

public class CustomHelloListener extends HelloBaseListener {
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterR(HelloParser.RContext ctx) {
        System.out.println("Entering rule r");
        System.out.println("name = " + ctx.ID().getText());
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitR(HelloParser.RContext ctx) {
        System.out.println("Exiting rule r");
    }
}
