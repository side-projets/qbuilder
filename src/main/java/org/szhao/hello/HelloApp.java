package org.szhao.hello;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.szhao.hello.autogen.HelloLexer;
import org.szhao.hello.autogen.HelloParser;

import java.io.IOException;

/**
 * author: zhaoshilong
 * date: 2019-05-10
 */
public class HelloApp {
    public static void main(String[] args) throws IOException {
//        ANTLRInputStream input = new ANTLRInputStream(System.in);
        System.out.println("Hello app ...");
        CharStream input = CharStreams.fromStream(System.in);

        HelloLexer lexer = new HelloLexer(input);

        CommonTokenStream tokens = new CommonTokenStream(lexer);

        HelloParser parser = new HelloParser(tokens);

        ParseTree tree = parser.r();

//        System.out.println(tree.toStringTree(parser));

        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(new CustomHelloListener(), tree);
        System.out.println();
    }
}
