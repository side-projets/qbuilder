package org.szhao.init;

import org.szhao.init.autogen.ArrayInitBaseListener;
import org.szhao.init.autogen.ArrayInitParser;

public class CustomArrayInitListener extends ArrayInitBaseListener {
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterInit(ArrayInitParser.InitContext ctx) {
        System.out.print('"');
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitInit(ArrayInitParser.InitContext ctx) {
        System.out.print('"');
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void enterValue(ArrayInitParser.ValueContext ctx) {
        int value = Integer.valueOf(ctx.INT().getText());
        System.out.printf("\\u%04x", value);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitValue(ArrayInitParser.ValueContext ctx) {

    }

}
