package org.szhao.init;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.szhao.init.autogen.ArrayInitLexer;
import org.szhao.init.autogen.ArrayInitParser;


import java.io.IOException;

public class ArrayInitApp {

    public static void main(String[] args) throws IOException {
        System.out.println("ArrayInit app ...");

        CharStream input = CharStreams.fromStream(System.in);

        ArrayInitLexer lexer = new ArrayInitLexer(input);

        CommonTokenStream tokens = new CommonTokenStream(lexer);

        ArrayInitParser parser = new ArrayInitParser(tokens);

        ParseTree tree = parser.init();

        System.out.println(tree.toStringTree(parser));

        // TODO: cannot process recursive cases {12,23,{12,23},34}
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(new CustomArrayInitListener(), tree);
        System.out.println();
    }
}
