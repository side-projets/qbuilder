package org.szhao.query;


import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.szhao.query.autogen.QueryInURLBaseListener;
import org.szhao.query.autogen.QueryInURLParser;

public class Query2JPATranslator extends QueryInURLBaseListener {
    private ParseTreeProperty<String> jpa = new ParseTreeProperty<>();

    public void setJPA(ParseTree node, String s) {
        jpa.put(node, s);
    }

    public String getJPA(ParseTree node) {
        return jpa.get(node);
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     *
     * @param ctx
     */
    @Override
    public void exitBaseQuery(QueryInURLParser.BaseQueryContext ctx) {
        String q = String.format("root.%s %s %s",
                ctx.fieldName().getText(),
                ctx.comparisonOperator().getText(),
                ctx.STRING().getText());
        jpa.put(ctx, q);
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     *
     * @param ctx
     */
    @Override
    public void exitAndQuery(QueryInURLParser.AndQueryContext ctx) {
        String q0 = jpa.get(ctx.query(0));
        String q1 = jpa.get(ctx.query(1));
        jpa.put(ctx, String.format("%s AND %s", q0, q1));
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     *
     * @param ctx
     */
    @Override
    public void exitParenthesisQuery(QueryInURLParser.ParenthesisQueryContext ctx) {
        String q = jpa.get(ctx.query());
        jpa.put(ctx, String.format("(%s)", q));
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     *
     * @param ctx
     */
    @Override
    public void exitOrQuery(QueryInURLParser.OrQueryContext ctx) {
        String q0 = jpa.get(ctx.query(0));
        String q1 = jpa.get(ctx.query(1));
        jpa.put(ctx, String.format("%s OR %s", q0, q1));
    }
}
