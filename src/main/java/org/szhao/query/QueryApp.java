package org.szhao.query;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.szhao.query.autogen.QueryInURLLexer;
import org.szhao.query.autogen.QueryInURLParser;

import java.io.IOException;

import static java.lang.System.*;

public class QueryApp {
    public static void main(String[] args) throws IOException {
        CharStream is = CharStreams.fromStream(in);
        QueryInURLLexer lexer = new QueryInURLLexer(is);
        TokenStream tokens = new CommonTokenStream(lexer);
        QueryInURLParser parser = new QueryInURLParser(tokens);
        parser.setBuildParseTree(true);
        ParseTree tree = parser.query();
//        System.out.println(tree.toStringTree(parser));

        ParseTreeWalker walker = new ParseTreeWalker();
        Query2JPATranslator translator = new Query2JPATranslator();
        walker.walk(translator, tree);
        System.out.println(translator.getJPA(tree));
    }
}
