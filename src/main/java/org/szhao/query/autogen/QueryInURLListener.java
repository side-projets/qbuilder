// Generated from QueryInURL.g4 by ANTLR 4.7.2
package org.szhao.query.autogen;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link QueryInURLParser}.
 */
public interface QueryInURLListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by the {@code baseQuery}
	 * labeled alternative in {@link QueryInURLParser#query}.
	 * @param ctx the parse tree
	 */
	void enterBaseQuery(QueryInURLParser.BaseQueryContext ctx);
	/**
	 * Exit a parse tree produced by the {@code baseQuery}
	 * labeled alternative in {@link QueryInURLParser#query}.
	 * @param ctx the parse tree
	 */
	void exitBaseQuery(QueryInURLParser.BaseQueryContext ctx);
	/**
	 * Enter a parse tree produced by the {@code andQuery}
	 * labeled alternative in {@link QueryInURLParser#query}.
	 * @param ctx the parse tree
	 */
	void enterAndQuery(QueryInURLParser.AndQueryContext ctx);
	/**
	 * Exit a parse tree produced by the {@code andQuery}
	 * labeled alternative in {@link QueryInURLParser#query}.
	 * @param ctx the parse tree
	 */
	void exitAndQuery(QueryInURLParser.AndQueryContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parenthesisQuery}
	 * labeled alternative in {@link QueryInURLParser#query}.
	 * @param ctx the parse tree
	 */
	void enterParenthesisQuery(QueryInURLParser.ParenthesisQueryContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parenthesisQuery}
	 * labeled alternative in {@link QueryInURLParser#query}.
	 * @param ctx the parse tree
	 */
	void exitParenthesisQuery(QueryInURLParser.ParenthesisQueryContext ctx);
	/**
	 * Enter a parse tree produced by the {@code orQuery}
	 * labeled alternative in {@link QueryInURLParser#query}.
	 * @param ctx the parse tree
	 */
	void enterOrQuery(QueryInURLParser.OrQueryContext ctx);
	/**
	 * Exit a parse tree produced by the {@code orQuery}
	 * labeled alternative in {@link QueryInURLParser#query}.
	 * @param ctx the parse tree
	 */
	void exitOrQuery(QueryInURLParser.OrQueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link QueryInURLParser#fieldName}.
	 * @param ctx the parse tree
	 */
	void enterFieldName(QueryInURLParser.FieldNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link QueryInURLParser#fieldName}.
	 * @param ctx the parse tree
	 */
	void exitFieldName(QueryInURLParser.FieldNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link QueryInURLParser#comparisonOperator}.
	 * @param ctx the parse tree
	 */
	void enterComparisonOperator(QueryInURLParser.ComparisonOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link QueryInURLParser#comparisonOperator}.
	 * @param ctx the parse tree
	 */
	void exitComparisonOperator(QueryInURLParser.ComparisonOperatorContext ctx);
}