package org.szhao.expr;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.szhao.expr.autogen.ExprLexer;
import org.szhao.expr.autogen.ExprParser;

import java.io.IOException;
import java.io.InputStream;

public class ExprApp {
    public static void main(String[] args) throws IOException {
        System.out.println("Expression app ... ");
        InputStream is = System.in;
        CharStream input = CharStreams.fromStream(is);
        ExprLexer lexer = new ExprLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        ExprParser parser = new ExprParser(tokens);
        ParseTree tree = parser.prog();
        System.out.println(tree.toStringTree(parser));
    }
}
