grammar QueryInURL;

query
: fieldName comparisonOperator STRING           # baseQuery
| query '&&' query                              # andQuery
| query '||' query                              # orQuery
| '(' query ')'                                 # parenthesisQuery
;

fieldName: ID('.'ID)* ;

comparisonOperator:  '<' | '<=' | '<>' | '>' | '>=' | '==' ;

ID
: [a-zA-Z][a-zA-Z0-9_]*
;

STRING :  '"' (ESC | ~["\\])* '"' ;
fragment ESC :   '\\' (["\\/bfnrt] | UNICODE) ;
fragment UNICODE : 'u' HEX HEX HEX HEX ;
fragment HEX : [0-9a-fA-F] ;

WS  :   [ \t\n\r]+ -> skip ;