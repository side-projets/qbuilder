grammar LabeledExpr;

prog: stat+;
stat: expr NEWLINE      # printExpr
| ID '=' expr NEWLINE   # assign
| NEWLINE               # blank
;

expr: expr op=('*'|'/') expr    # mulDiv
| expr op=('+'|'-') expr        # addSub
| INT                           # int
| ID                            # id
| '('expr')'                    # parens
;

MUL:'*';
DIV: '/';
ADD: '+';
SUB: '-';
ID: [a-zA-Z]+;
INT: [0-9]+;
WS: [ \t]+ -> skip;
NEWLINE: '\r'? '\n';
